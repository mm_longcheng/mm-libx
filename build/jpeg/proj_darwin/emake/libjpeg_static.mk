LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libjpeg_static
LOCAL_MODULE_FILENAME := libjpeg_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/jpeg
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/jpeg
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/jmemdos.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/jmemmac.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/jmemname.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/jmemansi.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/ckconfig.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/ansi2knr.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/example.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/cjpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/cdjpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/ckconfig.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/jpegtran.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/djpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/rdjpgcom.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/wrjpgcom.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/rdrle.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/wrrle.c
MY_SOURCES_FILTER_OUT += ../../../../src/jpeg/jmemdosa.asm

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
