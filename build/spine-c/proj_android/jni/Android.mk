LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(CLEAR_VARS)

include $(LOCAL_PATH)/libspine-c_static.mk
include $(LOCAL_PATH)/libspine-c_shared.mk

