LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libprotoc_shared
LOCAL_MODULE_FILENAME := libprotoc_shared
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-inconsistent-missing-override
LOCAL_CFLAGS += -Wno-unused-private-field

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti 
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libprotobuf_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/protobuf/src
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/protobuf/src/google/protobuf/compiler
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/annotation_test_util.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/command_line_interface_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/importer_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/parser_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/zip_output_unittest.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/cpp/cpp_bootstrap_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/cpp/cpp_move_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/cpp/cpp_plugin_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/cpp/cpp_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/cpp/metadata_test.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/csharp/csharp_bootstrap_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/csharp/csharp_generator_unittest.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/java/java_doc_comment_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/java/java_plugin_unittest.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/objectivec/objectivec_helpers_unittest.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/python/python_plugin_unittest.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/ruby/ruby_generator_unittest.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/importer.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/parser.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/main.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/mock_code_generator.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/test_plugin.cc

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
