LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libprotobuf-lite_shared
LOCAL_MODULE_FILENAME := libprotobuf-lite_shared
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DHAVE_PTHREAD=1
LOCAL_CFLAGS += -DHAVE_ZLIB=1

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti 
########################################################################
LOCAL_LDLIBS += -fPIC
LOCAL_LDLIBS += -llog
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/protobuf/src

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/any_lite.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/arena.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/arenastring.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/extension_set.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/generated_enum_util.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/generated_message_table_driven_lite.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/generated_message_util.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/implicit_weak_message.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/io/coded_stream.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/io/strtod.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/io/zero_copy_stream.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/io/zero_copy_stream_impl.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/io/zero_copy_stream_impl_lite.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/map.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/message_lite.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/parse_context.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/repeated_field.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/stubs/bytestream.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/stubs/common.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/stubs/int128.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/stubs/status.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/stubs/statusor.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/stubs/stringpiece.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/stubs/stringprintf.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/stubs/structurally_valid.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/stubs/strutil.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/stubs/time.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/wire_format_lite.cc
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
# MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/protobuf/src
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################