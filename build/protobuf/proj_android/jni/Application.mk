APP_ABI := arm64-v8a armeabi-v7a # armeabi

APP_STL := c++_shared

APP_PLATFORM := android-16

APP_MODULES += libprotobuf_static
APP_MODULES += libprotobuf_shared

APP_MODULES += libprotobuf-lite_static
APP_MODULES += libprotobuf-lite_shared

APP_MODULES += libprotoc_static
APP_MODULES += libprotoc_shared

APP_MODULES += protoc
