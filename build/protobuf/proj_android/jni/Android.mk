LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(CLEAR_VARS)

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libprotobuf_static.mk
include $(LOCAL_PATH)/libprotobuf_shared.mk

include $(LOCAL_PATH)/libprotobuf-lite_static.mk
include $(LOCAL_PATH)/libprotobuf-lite_shared.mk

include $(LOCAL_PATH)/libprotoc_static.mk
include $(LOCAL_PATH)/libprotoc_shared.mk

include $(LOCAL_PATH)/protoc.mk
