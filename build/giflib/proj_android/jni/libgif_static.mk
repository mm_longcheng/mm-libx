LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libgif_static
LOCAL_MODULE_FILENAME := libgif_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/giflib
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/giflib
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/gif2rgb.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/gifbg.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/gifbuild.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/gifclrmp.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/gifcolor.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/gifecho.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/giffilter.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/giffix.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/gifhisto.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/gifinto.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/gifsponge.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/giftext.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/giftool.c
MY_SOURCES_FILTER_OUT += ../../../../src/giflib/gifwedge.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################