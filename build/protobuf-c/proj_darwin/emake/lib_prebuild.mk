LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_shared
LOCAL_SRC_FILES := ../../../zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_static
LOCAL_SRC_FILES := ../../../zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libprotobuf_shared
LOCAL_SRC_FILES := ../../../protobuf/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libprotobuf_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libprotobuf_static
LOCAL_SRC_FILES := ../../../protobuf/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libprotobuf_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libprotoc_shared
LOCAL_SRC_FILES := ../../../protobuf/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libprotoc_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libprotoc_static
LOCAL_SRC_FILES := ../../../protobuf/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libprotoc_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################