LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := protoc-c
LOCAL_MODULE_FILENAME := protoc-c
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-unused-private-field

LOCAL_CFLAGS += -DPROTOC_C__USE_CAMELCASE_NAME

LOCAL_CFLAGS += -DPACKAGE_VERSION="\"1.4.0\""
LOCAL_CFLAGS += -DPACKAGE_STRING="\"protobuf-c 1.4.0\""

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti 
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += libprotoc_static
LOCAL_STATIC_LIBRARIES += libprotobuf_static
LOCAL_STATIC_LIBRARIES += libz_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/protobuf-c
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/protobuf-c/protobuf-c
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/protobuf/src
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/protobuf-c/protoc-c
MY_SOURCES_PATH += $(LOCAL_PATH)/../../source

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
