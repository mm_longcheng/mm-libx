############################
# libjxr
############################
############################
LOCAL_CFLAGS += -Wno-comment
LOCAL_CFLAGS += -Wno-dangling-else
LOCAL_CFLAGS += -Wno-unused-const-variable
LOCAL_CFLAGS += -Wno-unused-value
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-shift-negative-value
LOCAL_CFLAGS += -Wno-constant-conversion
LOCAL_CFLAGS += -Wno-deprecated-declarations
LOCAL_CFLAGS += -Wno-unknown-pragmas

LOCAL_CFLAGS += -D__ANSI__
LOCAL_CFLAGS += -DDISABLE_PERF_MEASUREMENT

