LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libjxr_shared
LOCAL_MODULE_FILENAME := libjxr_shared
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-comment
LOCAL_CFLAGS += -Wno-dangling-else
LOCAL_CFLAGS += -Wno-unused-const-variable
LOCAL_CFLAGS += -Wno-unused-value
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-shift-negative-value
LOCAL_CFLAGS += -Wno-constant-conversion
LOCAL_CFLAGS += -Wno-deprecated-declarations
LOCAL_CFLAGS += -Wno-unknown-pragmas

LOCAL_CFLAGS += -D__ANSI__
LOCAL_CFLAGS += -DDISABLE_PERF_MEASUREMENT
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/jxrlib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/jxrlib/common/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/jxrlib/image/sys
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/jxrlib/jxrgluelib
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/jxrlib/common
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/jxrlib/image
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/jxrlib/jxrgluelib
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/jxrlib/image/sys/perfTimerANSI.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
