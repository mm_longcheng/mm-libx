LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libtiff_static
LOCAL_MODULE_FILENAME := libtiff_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/tiff
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/tiff/libtiff
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/tiff/port
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)/libtiff
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)/port

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/jpeg
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/jpeg/include/$(MM_PLATFORM)

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/libwebp/src

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zstd/lib
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/tiff/libtiff
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/tiff/libtiff/tif_win32.c
MY_SOURCES_FILTER_OUT += ../../../../src/tiff/libtiff/mkg3states.c

MY_SOURCES_FILTER_OUT += ../../../../src/tiff/libtiff/tif_jbig.c
MY_SOURCES_FILTER_OUT += ../../../../src/tiff/libtiff/tif_jpeg_12.c
MY_SOURCES_FILTER_OUT += ../../../../src/tiff/libtiff/tif_lerc.c
MY_SOURCES_FILTER_OUT += ../../../../src/tiff/libtiff/tif_lzma.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################