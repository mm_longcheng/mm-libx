LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_shared
LOCAL_SRC_FILES := ../../../zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_static
LOCAL_SRC_FILES := ../../../zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libjpeg_shared
LOCAL_SRC_FILES := ../../../jpeg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libjpeg_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libjpeg_static
LOCAL_SRC_FILES := ../../../jpeg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libjpeg_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libwebp_shared
LOCAL_SRC_FILES := ../../../libwebp/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libwebp_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libwebp_static
LOCAL_SRC_FILES := ../../../libwebp/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libwebp_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libzstd_shared
LOCAL_SRC_FILES := ../../../zstd/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libzstd_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libzstd_static
LOCAL_SRC_FILES := ../../../zstd/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libzstd_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
