LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := iconv
LOCAL_MODULE_FILENAME := iconv
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DAndroid
LOCAL_CFLAGS += -DLIBDIR=\"/usr/local/lib\"
LOCAL_CFLAGS += -DBUILDING_LIBICONV
LOCAL_CFLAGS += -DHAVE_CONFIG_H
# LOCAL_CFLAGS += -DIN_LIBRARY

# LOCAL_CFLAGS += -DEXEEXT=\"\"
# LOCAL_CFLAGS += -DLIBDIRS=\"/usr/local/lib\",
# LOCAL_CFLAGS += -DLIBPATHVAR=\"\"
# LOCAL_CFLAGS += -DINSTALLDIR=\"\"
# LOCAL_CFLAGS += -DENABLE_RELOCATABLE

LOCAL_CXXFLAGS += 
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += libiconv_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/libiconv/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/libiconv/srclib

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/srclib

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/libcharset/lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/libcharset/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/libcharset
########################################################################
LOCAL_SRC_FILES  += ../../../../src/libiconv/src/iconv.c

LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/allocator.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/areadlink.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/basename-lgpl.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/binary-io.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/c-ctype.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/canonicalize-lgpl.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/careadlinkat.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/dirname-lgpl.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/error.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/getprogname.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/lstat.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/malloca.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/progname.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/progreloc.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/readlink.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/safe-read.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/stat-time.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/stat.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/strerror-override.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/strerror.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/stripslash.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/unistd.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/xmalloc.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/xreadlink.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/xstrdup.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/uniwidth/width.c

LOCAL_SRC_FILES  += ../../../../src/libiconv/srclib/relocatable.c
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
# MY_SOURCES_PATH += ../../../../src/libiconv/srclib

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
# MY_SOURCES_FILTER_OUT += ../../../../src/libiconv/srclib/msvc-nothrow.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
# $(modules-dump-database)
