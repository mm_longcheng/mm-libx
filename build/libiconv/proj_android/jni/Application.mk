APP_ABI := arm64-v8a armeabi-v7a

APP_STL := c++_shared

APP_PLATFORM := android-16

APP_MODULES += libiconv_static
APP_MODULES += libiconv_shared

APP_MODULES += iconv
