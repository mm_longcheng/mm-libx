/*
** $Id: port-ndk.c $
** save port ndk armeabi-v7a
** See Copyright Notice in lua.h
*/

#include <string.h>

#define _USE_MATH_DEFINES
#include <math.h>

#if !defined(LUA_USE_C89)
// android-ndk-r23b armeabi-v7a undefined symbol: log2
#if !defined(log2)
double log2(double x)
{
    return ((1. / M_LN2)*log(x));
}
#endif
#else
// android-ndk-r23b armeabi-v7a undefined symbol: stpcpy
#if !defined(stpcpy)
char* stpcpy(char* dest, const char* src)
{
	size_t len = strlen(src);
	return memcpy(dest, src, len + 1) + len;
}
#endif
#endif

