LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := openal-info
LOCAL_MODULE_FILENAME := openal-info
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
########################################################################
LOCAL_LDLIBS += -fPIC
LOCAL_LDLIBS += -framework Foundation
LOCAL_LDLIBS += -framework CoreAudio
LOCAL_LDLIBS += -framework AudioToolbox
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += libOpenAL_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openal-soft
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openal-soft/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openal-soft/alc
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openal-soft/common
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
########################################################################
LOCAL_SRC_FILES  += 
LOCAL_SRC_FILES  += ../../../../src/openal-soft/utils/openal-info.c
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += 

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
