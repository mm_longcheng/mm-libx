LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libOpenAL_shared
LOCAL_MODULE_FILENAME := libOpenAL_shared
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

# LOCAL_CFLAGS += -mfpu=neon
LOCAL_CFLAGS += -DRESTRICT=__restrict
LOCAL_CFLAGS += -DAL_BUILD_LIBRARY
LOCAL_CFLAGS += -DAL_ALEXT_PROTOTYPES
LOCAL_CFLAGS += -DNOMINMAX
LOCAL_CFLAGS += -DAL_API="__attribute__ ((visibility(\"default\")))"
LOCAL_CFLAGS += -DALC_API="__attribute__ ((visibility(\"default\")))"

LOCAL_CXXFLAGS += -std=c++14
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += -fPIC
LOCAL_LDLIBS += -framework Foundation
LOCAL_LDLIBS += -framework CoreAudio
LOCAL_LDLIBS += -framework AudioToolbox
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openal-soft
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openal-soft/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openal-soft/alc
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openal-soft/common
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/openal-soft/al
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/openal-soft/common
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/openal-soft/core
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
# MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/uiddefs.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/alsa.cpp
# MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/coreaudio.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/dsound.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/jack.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/oboe.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/oss.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/opensl.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/portaudio.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/pulseaudio.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/sdl2.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/sndio.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/solaris.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/wasapi.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/winmm.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/alc/backends/pipewire.cpp
# MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/core/mixer/mixer_sse.cpp
# MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/core/mixer/mixer_sse2.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/core/mixer/mixer_sse3.cpp
# MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/core/mixer/mixer_sse41.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/core/mixer/mixer_neon.cpp

MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/core/dbus_wrap.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/core/rtkit.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/core/uiddefs.cpp

MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/common/alfstream.cpp

MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/al/eax_api.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/al/eax_eax_call.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/al/eax_effect.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/al/eax_exception.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/al/eax_fx_slot_index.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/al/eax_fx_slots.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/al/eax_globals.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/al/eax_utils.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/al/eax_x_ram.cpp

MY_SOURCES_FILTER_OUT += ../../../../src/openal-soft/al/effects/effects.cpp

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################