LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libssl_shared
LOCAL_MODULE_FILENAME := libssl_shared
########################################################################
LOCAL_CFLAGS += -fPIC

INSTALLTOP := /usr/local
OPENSSLDIR := /usr/local/ssl
LIBDIR := lib
ENGINESDIR := /usr/local/lib/engines-1.1

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DNDEBUG 
LOCAL_CFLAGS += -DOPENSSL_THREADS 
LOCAL_CFLAGS += -DOPENSSL_NO_STATIC_ENGINE 
LOCAL_CFLAGS += -DOPENSSL_PIC 
LOCAL_CFLAGS += -DOPENSSLDIR="\"$(OPENSSLDIR)\"" 
LOCAL_CFLAGS += -DENGINESDIR="\"$(ENGINESDIR)\"" 
LOCAL_CFLAGS += -O3
LOCAL_CFLAGS += -pthread
LOCAL_CFLAGS += -Wa,--noexecstack 

LOCAL_CFLAGS += -DOPENSSL_USE_NODELETE
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libcrypto_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/crypto
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/crypto/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/crypto/modes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/crypto
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/crypto/include
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/openssl/ssl
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/ssl/ssl_utst.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/ssl/t1_trce.c

MY_SOURCES_EXTENSION += .cpp .c .cc
MY_SOURCES_EXTENSION += .s
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
