LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libcrypto_shared
LOCAL_MODULE_FILENAME := libcrypto_shared
########################################################################
LOCAL_CFLAGS += -fPIC

INSTALLTOP := /usr/local
OPENSSLDIR := /usr/local/ssl
LIBDIR := lib
ENGINESDIR := /usr/local/lib/engines-1.1

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DNDEBUG 
LOCAL_CFLAGS += -DOPENSSL_THREADS 
LOCAL_CFLAGS += -DOPENSSL_NO_STATIC_ENGINE 
LOCAL_CFLAGS += -DOPENSSL_PIC 
LOCAL_CFLAGS += -DOPENSSLDIR="\"$(OPENSSLDIR)\"" 
LOCAL_CFLAGS += -DENGINESDIR="\"$(ENGINESDIR)\"" 
LOCAL_CFLAGS += -O3 
LOCAL_CFLAGS += -pthread
LOCAL_CFLAGS += -Wa,--noexecstack 

LOCAL_CFLAGS += -DOPENSSL_USE_NODELETE
LOCAL_CFLAGS += -DZLIB
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libz_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/crypto
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/crypto/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/crypto/modes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/crypto/ec/curve448
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/crypto/ec/curve448/arch_32
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/crypto
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/crypto/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/crypto
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/openssl/crypto
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
# MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/aria/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/md2/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/rc5/%

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/aes/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/async/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/bf/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/bn/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/camellia/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/cast/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/chacha/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/des/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/ec/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/md5/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/modes/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/poly1350/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/rc4/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/rc5/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/ripemd/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/sha/asm/%
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/whrlpool/asm/%

# MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/aes/aes_cbc.c
# MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/aes/aes_core.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/aes/aes_x86core.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/des/ncbc_enc.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/ec/ecp_nistz256.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/ec/ecp_nistz256_table.c

# MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/bn/bn_asm.c

# MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/chacha/chacha_enc.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/poly1305/poly1305_base2_44.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/poly1305/poly1305_ieee754.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/LPdir_nyi.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/LPdir_vms.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/LPdir_win.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/LPdir_win32.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/LPdir_wince.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/LPdir_unix.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/sparcv9cap.c
# MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/mem_clr.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/ppccap.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/s390xcap.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/sparcv9cap.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/armcap.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/engine/eng_devcrypto.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/dllmain.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/ebcdic.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/threads_none.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/threads_win.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/async/arch/async_null.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/async/arch/async_win.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/bn/rsaz_exp.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/dso/dso_dl.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/dso/dso_openssl.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/dso/dso_vms.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/dso/dso_win32.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/ec/ecp_nistp224.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/ec/ecp_nistp256.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/ec/ecp_nistp521.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/ec/ecp_nistputil.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/evp/e_rc5.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/evp/m_md2.c

MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/rand/rand_egd.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/rand/rand_vms.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/crypto/rand/rand_win.c

MY_SOURCES_EXTENSION += .cpp .c .cc
MY_SOURCES_EXTENSION += .s
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
