LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := openssl
LOCAL_MODULE_FILENAME := openssl
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += libz_static
LOCAL_STATIC_LIBRARIES += libssl_static
LOCAL_STATIC_LIBRARIES += libcrypto_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/crypto
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/crypto/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/crypto/modes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/crypto
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/crypto/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/$(MM_PLATFORM)/apps
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += ../../../../src/openssl/apps

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/openssl/apps/win32_init.c

MY_SOURCES_EXTENSION += .cpp .c .cc
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
