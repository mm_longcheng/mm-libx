LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_shared
LOCAL_SRC_FILES := ../../../zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_static
LOCAL_SRC_FILES := ../../../zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libjpeg_shared
LOCAL_SRC_FILES := ../../../jpeg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libjpeg_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libjpeg_static
LOCAL_SRC_FILES := ../../../jpeg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libjpeg_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libwebp_shared
LOCAL_SRC_FILES := ../../../libwebp/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libwebp_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libwebp_static
LOCAL_SRC_FILES := ../../../libwebp/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libwebp_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libpng_shared
LOCAL_SRC_FILES := ../../../libpng/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libpng_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libpng_static
LOCAL_SRC_FILES := ../../../libpng/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libpng_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libtiff_shared
LOCAL_SRC_FILES := ../../../tiff/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libtiff_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libtiff_static
LOCAL_SRC_FILES := ../../../tiff/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libtiff_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libopenjp2_shared
LOCAL_SRC_FILES := ../../../openjpeg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libopenjp2_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libopenjp2_static
LOCAL_SRC_FILES := ../../../openjpeg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libopenjp2_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libjxr_shared
LOCAL_SRC_FILES := ../../../jxrlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libjxr_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libjxr_static
LOCAL_SRC_FILES := ../../../jxrlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libjxr_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libRaw_shared
LOCAL_SRC_FILES := ../../../LibRaw/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libRaw_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libRaw_static
LOCAL_SRC_FILES := ../../../LibRaw/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libRaw_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libImath_shared
LOCAL_SRC_FILES := ../../../Imath/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libImath_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libImath_static
LOCAL_SRC_FILES := ../../../Imath/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libImath_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libIex_shared
LOCAL_SRC_FILES := ../../../openexr/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libIex_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libIex_static
LOCAL_SRC_FILES := ../../../openexr/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libIex_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libOpenEXR_shared
LOCAL_SRC_FILES := ../../../openexr/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOpenEXR_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libOpenEXR_static
LOCAL_SRC_FILES := ../../../openexr/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOpenEXR_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
