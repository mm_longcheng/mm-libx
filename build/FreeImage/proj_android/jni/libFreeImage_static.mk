LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libFreeImage_static
LOCAL_MODULE_FILENAME := libFreeImage_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-unused-private-field
LOCAL_CFLAGS += -Wno-char-subscripts
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-unused-const-variable

LOCAL_CFLAGS += -D__ANSI__
LOCAL_CFLAGS += -DFREEIMAGE_COLORORDER=FREEIMAGE_COLORORDER_RGB

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
LOCAL_CFLAGS += 
else
LOCAL_CFLAGS += -D_LIBCPP_HAS_NO_OFF_T_FUNCTIONS
endif

LOCAL_CXXFLAGS += -std=c++11
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/tiff/include/xcode
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/tiff/include/xcode/libtiff
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/jpeg/include/xcode
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/openexr/include/xcode
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/Imath/include/xcode
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/openjpeg/include/xcode
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/libpng/include/xcode
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/tiff
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/jpeg
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openexr/src/lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openexr/src/lib/Iex
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Imath/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Imath/src/Imath
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openjpeg/src/lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/LibRaw
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/libpng
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/jxrlib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/jxrlib/jxrgluelib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/jxrlib/image/sys
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/jxrlib/common/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/libwebp/src
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/FreeImage/FreeImageC.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################