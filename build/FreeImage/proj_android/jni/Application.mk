APP_ABI := arm64-v8a armeabi-v7a # armeabi

APP_STL := c++_shared

APP_PLATFORM := android-16

APP_MODULES += libFreeImage_static
APP_MODULES += libFreeImage_shared
