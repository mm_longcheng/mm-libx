LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(CLEAR_VARS)

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libFreeImage_static.mk
include $(LOCAL_PATH)/libFreeImage_shared.mk
