LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libz_static
LOCAL_MODULE_FILENAME := libz_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-shift-negative-value
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += ../../../../src/zlib/adler32.c
LOCAL_SRC_FILES  += ../../../../src/zlib/compress.c
LOCAL_SRC_FILES  += ../../../../src/zlib/crc32.c
LOCAL_SRC_FILES  += ../../../../src/zlib/deflate.c
LOCAL_SRC_FILES  += ../../../../src/zlib/gzclose.c
LOCAL_SRC_FILES  += ../../../../src/zlib/gzlib.c
LOCAL_SRC_FILES  += ../../../../src/zlib/gzread.c
LOCAL_SRC_FILES  += ../../../../src/zlib/gzwrite.c
LOCAL_SRC_FILES  += ../../../../src/zlib/inflate.c
LOCAL_SRC_FILES  += ../../../../src/zlib/infback.c
LOCAL_SRC_FILES  += ../../../../src/zlib/inftrees.c
LOCAL_SRC_FILES  += ../../../../src/zlib/inffast.c
LOCAL_SRC_FILES  += ../../../../src/zlib/trees.c
LOCAL_SRC_FILES  += ../../../../src/zlib/uncompr.c
LOCAL_SRC_FILES  += ../../../../src/zlib/zutil.c
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
