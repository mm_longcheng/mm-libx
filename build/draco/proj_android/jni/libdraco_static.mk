LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libdraco_static
LOCAL_MODULE_FILENAME := libdraco_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unused-local-typedef
LOCAL_CFLAGS += -Wno-range-loop-analysis

LOCAL_CXXFLAGS += -std=c++11
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/draco
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/draco/src

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/draco/src
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/draco/src/draco/tools%
MY_SOURCES_FILTER_OUT += ../../../../src/draco/src/draco/javascript%
MY_SOURCES_FILTER_OUT += ../../../../src/draco/src/draco/%_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/draco/src/draco/core/draco_test_utils.cc

MY_SOURCES_FILTER_OUT += ../../../../src/draco/src/draco/maya%
MY_SOURCES_FILTER_OUT += ../../../../src/draco/src/draco/unity%

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################