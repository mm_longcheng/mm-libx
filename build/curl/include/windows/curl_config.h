/***************************************************************************
 *                                  _   _ ____  _
 *  Project                     ___| | | |  _ \| |
 *                             / __| | | | |_) | |
 *                            | (__| |_| |  _ <| |___
 *                             \___|\___/|_| \_\_____|
 *
 * Copyright (C) 1998 - 2021, Daniel Stenberg, <daniel@haxx.se>, et al.
 *
 * This software is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at https://curl.se/docs/copyright.html.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell
 * copies of the Software, and permit persons to whom the Software is
 * furnished to do so, under the terms of the COPYING file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 ***************************************************************************/

#ifndef HEADER_CURL_CONFIG_H
#define HEADER_CURL_CONFIG_H

#include "config-win32.h"

/* to enable zlib support */
#define HAVE_LIBZ 1
#define HAVE_ZLIB_H 1

/* to enable rtmp support */
// #define HAVE_LIBRTMP_RTMP_H 1
// #define USE_LIBRTMP 1

/* to enable schannel support */
#define USE_SCHANNEL
/* to enable SSPI support */
#define USE_WINDOWS_SSPI 1
#define USE_SPNEGO

/* to enable OpenSSL support */
#define USE_OPENSSL 1
#define HAVE_OPENSSL_ENGINE_H 1

#define USE_WIN32_IDN

#endif /* HEADER_CURL_CONFIG_H */
