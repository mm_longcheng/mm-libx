LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libcurl_shared
LOCAL_MODULE_FILENAME := libcurl_shared
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DHAVE_CONFIG_H
LOCAL_CFLAGS += -DBUILDING_LIBCURL
LOCAL_CFLAGS += -DCURL_HIDDEN_SYMBOLS
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libssl_shared
LOCAL_SHARED_LIBRARIES += libcrypto_shared
LOCAL_SHARED_LIBRARIES += libz_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/curl/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/curl/lib

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/openssl/include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/curl/lib
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/ldap.c

MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/amigaos.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/asyn-ares.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/curl_des.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/curl_gssapi.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/curl_multibyte.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/curl_rtmp.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/curl_sspi.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/hostip4.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/hostsyn.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/http_negotiate.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/idn_win32.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/krb5.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/ldap.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/memdebug.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/non-ascii.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/nwlib.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/nwos.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/openldap.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/security.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/socks_gssapi.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/socks_sspi.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/ssh-libssh.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/ssh.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/strtok.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/system_win32.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/x509asn1.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/c-hyper.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/curl_ntlm_wb.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/curl_path.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/inet_ntop.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/inet_pton.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/psl.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/version_win32.c

MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vauth/digest_sspi.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vauth/krb5_gssapi.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vauth/krb5_sspi.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vauth/ntlm_sspi.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vauth/spnego_gssapi.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vauth/spnego_sspi.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vauth/gsasl.c

MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/axtls.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/cyassl.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/darwinssl.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/gskit.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/gtls.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/mbedtls.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/nss.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/polarssl.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/polarssl_threadlock.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/schannel.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/bearssl.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/mbedtls_threadlock.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/mesalink.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/rustls.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/schannel_verify.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/sectransp.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vtls/wolfssl.c

MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vquic/%
MY_SOURCES_FILTER_OUT += ../../../../src/curl/lib/vssh/%

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################