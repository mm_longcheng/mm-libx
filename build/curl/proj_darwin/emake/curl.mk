LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := curl
LOCAL_MODULE_FILENAME := curl
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DHAVE_CONFIG_H
LOCAL_CFLAGS += -DCURL_HIDDEN_SYMBOLS
LOCAL_CFLAGS += -DCURL_STATICLIB
########################################################################
LOCAL_LDLIBS += -fPIC
LOCAL_LDLIBS += -framework Foundation
LOCAL_LDLIBS += -framework SystemConfiguration
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += libcurl_static
LOCAL_STATIC_LIBRARIES += libssl_static
LOCAL_STATIC_LIBRARIES += libcrypto_static
LOCAL_STATIC_LIBRARIES += libz_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/curl/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/curl/lib

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/openssl/include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += ../../../../src/curl/lib/strtoofft.c
LOCAL_SRC_FILES  += ../../../../src/curl/lib/nonblock.c
LOCAL_SRC_FILES  += ../../../../src/curl/lib/warnless.c
LOCAL_SRC_FILES  += ../../../../src/curl/lib/curl_ctype.c
LOCAL_SRC_FILES  += ../../../../src/curl/lib/curl_multibyte.c
LOCAL_SRC_FILES  += ../../../../src/curl/lib/version_win32.c
LOCAL_SRC_FILES  += ../../../../src/curl/lib/dynbuf.c
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += ../../../../src/curl/src

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/curl/src/macos%

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
