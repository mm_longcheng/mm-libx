LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libopenjp2_static
LOCAL_MODULE_FILENAME := libopenjp2_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unused-function

# ndk 23.1.7779620
LOCAL_CFLAGS += -Wno-implicit-const-int-float-conversion
LOCAL_CFLAGS += -Wno-unknown-warning-option

LOCAL_CFLAGS += -DUSE_JPIP
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openjpeg/src/lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/openjpeg/src/lib/openjp2
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/openjpeg/src/lib/openjp2/bench_dwt.c
MY_SOURCES_FILTER_OUT += ../../../../src/openjpeg/src/lib/openjp2/t1_generate_luts.c
MY_SOURCES_FILTER_OUT += ../../../../src/openjpeg/src/lib/openjp2/test_sparse_array.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################