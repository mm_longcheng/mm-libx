APP_ABI := arm64-v8a armeabi-v7a

APP_STL := c++_shared

APP_PLATFORM := android-16

APP_MODULES += liblz4_static
APP_MODULES += liblz4_shared
