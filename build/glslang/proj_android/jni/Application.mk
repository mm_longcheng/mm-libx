APP_ABI := arm64-v8a armeabi-v7a # armeabi

APP_STL := c++_shared

APP_PLATFORM := android-16

APP_MODULES += libglslang_static
APP_MODULES += libglslang_shared

APP_MODULES += libSPIRV_static
APP_MODULES += libSPIRV_shared

APP_MODULES += glslangValidator
APP_MODULES += spirv-remap
