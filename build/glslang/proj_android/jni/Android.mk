LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(CLEAR_VARS)

include $(LOCAL_PATH)/libglslang_static.mk
include $(LOCAL_PATH)/libglslang_shared.mk

include $(LOCAL_PATH)/libSPIRV_static.mk
include $(LOCAL_PATH)/libSPIRV_shared.mk

include $(LOCAL_PATH)/glslangValidator.mk
include $(LOCAL_PATH)/spirv-remap.mk