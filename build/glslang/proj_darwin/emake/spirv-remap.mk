LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := spirv-remap
LOCAL_MODULE_FILENAME := spirv-remap
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DENABLE_HLSL
LOCAL_CFLAGS += -DENABLE_OPT=0

LOCAL_CXXFLAGS += -std=c++11
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += libSPIRV_static
LOCAL_STATIC_LIBRARIES += libglslang_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/glslang
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/glslang/StandAlone

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/glslang/StandAlone/StandAlone.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/glslang/StandAlone/resource_limits_c.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/glslang/StandAlone/ResourceLimits.cpp

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
