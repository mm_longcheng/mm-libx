LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libIlmThread_shared
LOCAL_MODULE_FILENAME := libIlmThread_shared
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-tautological-compare
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-deprecated-declarations

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti 
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libz_shared
LOCAL_SHARED_LIBRARIES += libImath_shared
LOCAL_SHARED_LIBRARIES += libIex_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openexr
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openexr/src/lib/Iex
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openexr/src/lib/OpenEXR
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openexr/src/lib/IlmThread
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Imath/src/Imath
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/Imath/include/$(MM_PLATFORM)

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/openexr/src/lib/IlmThread
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/openexr/src/lib/IlmThread/IlmThreadSemaphore.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openexr/src/lib/IlmThread/IlmThreadSemaphorePosix.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openexr/src/lib/IlmThread/IlmThreadSemaphorePosixCompat.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/openexr/src/lib/IlmThread/IlmThreadSemaphoreWin32.cpp

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
