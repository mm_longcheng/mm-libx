APP_ABI := arm64-v8a armeabi-v7a # armeabi

APP_STL := c++_shared

APP_PLATFORM := android-16

APP_MODULES += libIex_static
APP_MODULES += libIex_shared

APP_MODULES += libIlmThread_static
APP_MODULES += libIlmThread_shared

APP_MODULES += libOpenEXR_static
APP_MODULES += libOpenEXR_shared

APP_MODULES += libOpenEXRUtil_static
APP_MODULES += libOpenEXRUtil_shared
