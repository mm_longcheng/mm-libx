LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(CLEAR_VARS)

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libIex_static.mk
include $(LOCAL_PATH)/libIex_shared.mk

include $(LOCAL_PATH)/libIlmThread_static.mk
include $(LOCAL_PATH)/libIlmThread_shared.mk

include $(LOCAL_PATH)/libOpenEXR_static.mk
include $(LOCAL_PATH)/libOpenEXR_shared.mk

include $(LOCAL_PATH)/libOpenEXRUtil_static.mk
include $(LOCAL_PATH)/libOpenEXRUtil_shared.mk
