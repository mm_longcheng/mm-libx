LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(CLEAR_VARS)

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libtolua++_static.mk
include $(LOCAL_PATH)/libtolua++_shared.mk

