LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := liblua_shared
LOCAL_SRC_FILES := ../../../lua/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/liblua_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := liblua_static
LOCAL_SRC_FILES := ../../../lua/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/liblua_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################