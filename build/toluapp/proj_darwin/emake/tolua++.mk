LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := tolua++
LOCAL_MODULE_FILENAME := tolua++
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += libtolua++_static
LOCAL_STATIC_LIBRARIES += liblua_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/toluapp/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/lua/src
########################################################################
LOCAL_SRC_FILES  += ../../../../src/toluapp/src/bin/toluabind.c
LOCAL_SRC_FILES  += ../../../../src/toluapp/src/bin/tolua.c
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
# MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/toluapp/src/bin

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
