LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(CLEAR_VARS)

include $(LOCAL_PATH)/libsqlite_static.mk
include $(LOCAL_PATH)/libsqlite_shared.mk

