LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libsqlite_static
LOCAL_MODULE_FILENAME := libsqlite_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DSQLITE_ENABLE_DESERIALIZE
LOCAL_CFLAGS += -DSQLITE_ENABLE_DBSTAT_VTAB
LOCAL_CFLAGS += -DSQLITE_ENABLE_DBPAGE_VTAB
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/sqlite/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/sqlite/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../source
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/test%
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/os_win.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/mutex_w32.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/tclsqlite.c
MY_SOURCES_FILTER_OUT += ../../source/fts5.c
MY_SOURCES_FILTER_OUT += ../../source/fts5parse.c
MY_SOURCES_FILTER_OUT += ../../source/lempar.c
MY_SOURCES_FILTER_OUT += ../../source/shell.c

MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/mem0.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/mem2.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/mem3.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/mem5.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/notify.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/treeview.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/vdbevtab.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################