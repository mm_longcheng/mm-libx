LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_shared
LOCAL_SRC_FILES := ../../../openssl/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libcrypto_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_static
LOCAL_SRC_FILES := ../../../openssl/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libcrypto_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libssl_shared
LOCAL_SRC_FILES := ../../../openssl/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libssl_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libssl_static
LOCAL_SRC_FILES := ../../../openssl/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libssl_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################