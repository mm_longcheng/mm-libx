LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libzookeeper_static
LOCAL_MODULE_FILENAME := libzookeeper_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DTHREADED
LOCAL_CFLAGS += -DHAVE_OPENSSL_H
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zookeeper/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zookeeper/generated
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/openssl/include/$(MM_PLATFORM)
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/zookeeper
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/zookeeper/src/cli.c
MY_SOURCES_FILTER_OUT += ../../../../src/zookeeper/src/load_gen.c
MY_SOURCES_FILTER_OUT += ../../../../src/zookeeper/src/winport.c
MY_SOURCES_FILTER_OUT += ../../../../src/zookeeper/src/zk_sasl.c
MY_SOURCES_FILTER_OUT += ../../../../src/zookeeper/src/st_adaptor.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
