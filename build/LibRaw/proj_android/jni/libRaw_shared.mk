LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libRaw_shared
LOCAL_MODULE_FILENAME := libRaw_shared
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-comma
LOCAL_CFLAGS += -Wno-shorten-64-to-32
LOCAL_CFLAGS += -Wno-conditional-uninitialized
LOCAL_CFLAGS += -Wno-unreachable-code
LOCAL_CFLAGS += -Wno-strict-prototypes
LOCAL_CFLAGS += -Wno-constant-conversion
LOCAL_CFLAGS += -Wno-unused-const-variable
LOCAL_CFLAGS += -Wno-string-plus-int
LOCAL_CFLAGS += -Wno-dangling-else
LOCAL_CFLAGS += -Wno-parentheses

LOCAL_CFLAGS += -DNODEPS

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
LOCAL_CFLAGS += 
else
LOCAL_CFLAGS += -D_LIBCPP_HAS_NO_OFF_T_FUNCTIONS
endif

LOCAL_CXXFLAGS += -std=c++11
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/LibRaw
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/LibRaw/internal
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/LibRaw/src
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/LibRaw
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/dcraw%
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/src/decoders%
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/src/demosaic%
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/src/integration%
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/src/metadata%
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/src/postprocessing%
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/src/preprocessing%
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/src/tables%
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/src/utils%
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/src/write%
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/src/x3f%
MY_SOURCES_FILTER_OUT += ../../../../src/LibRaw/samples%

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################