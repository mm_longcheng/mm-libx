#ifndef CONFIG_H
#define CONFIG_H

#ifndef __cplusplus
# define inline inline
#endif

#ifdef __APPLE__
#include <TargetConditionals.h>
#endif
#if TARGET_OS_IPHONE
#define HAVE_HTONLL
#endif

/* #undef HAVE_SELECT */

#define HAVE_POLL

#define AMQ_PLATFORM "Darwin"

#endif /* CONFIG_H */
