#ifndef CONFIG_H
#define CONFIG_H

#ifndef __cplusplus
# define inline __inline
#endif

#define HAVE_HTONLL

#define HAVE_SELECT

/* #undef HAVE_POLL */

#define AMQ_PLATFORM "Windows"

#endif /* CONFIG_H */
