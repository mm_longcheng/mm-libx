#ifndef CONFIG_H
#define CONFIG_H

#ifndef __cplusplus
# define inline inline
#endif

/* #undef HAVE_HTONLL */

/* #undef HAVE_SELECT */

#define HAVE_POLL

#define AMQ_PLATFORM "android"

#endif /* CONFIG_H */
