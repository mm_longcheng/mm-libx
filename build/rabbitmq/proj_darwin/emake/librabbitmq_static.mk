LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := librabbitmq_static
LOCAL_MODULE_FILENAME := librabbitmq_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DHAVE_CONFIG_H
LOCAL_CFLAGS += -DAMQP_BUILD
LOCAL_CFLAGS += -Drabbitmq_EXPORTS
LOCAL_CFLAGS += -DENABLE_THREAD_SAFETY

LOCAL_CFLAGS += -Wno-unused-function
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/rabbitmq/librabbitmq
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/rabbitmq/librabbitmq/unix
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/$(MM_PLATFORM)/librabbitmq

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/openssl/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/openssl/include/$(MM_PLATFORM)
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/rabbitmq/librabbitmq
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/rabbitmq/librabbitmq/win32%

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
