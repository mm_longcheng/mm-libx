LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libzstd_shared
LOCAL_SRC_FILES := ../../../zstd/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libzstd_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libzstd_static
LOCAL_SRC_FILES := ../../../zstd/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libzstd_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################