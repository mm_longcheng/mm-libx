LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libogg_shared
LOCAL_SRC_FILES := ../../../libogg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libogg_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libogg_static
LOCAL_SRC_FILES := ../../../libogg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libogg_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################