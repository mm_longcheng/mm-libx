LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libktx_static
LOCAL_MODULE_FILENAME := libktx_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DLIBKTX
LOCAL_CFLAGS += -DKTX_FEATURE_KTX1
LOCAL_CFLAGS += -DKTX_FEATURE_KTX2
LOCAL_CFLAGS += -DKTX_FEATURE_WRITE

LOCAL_CXXFLAGS += -std=c++11
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/KTX-Software/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/KTX-Software/lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/KTX-Software/other_include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/KTX-Software/utils

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zstd/lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/dfdutils
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/basis_universal
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/KTX-Software/lib
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/KTX-Software/utils
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/KTX-Software/lib/astc-encoder%
MY_SOURCES_FILTER_OUT += ../../../../src/KTX-Software/lib/basisu%
MY_SOURCES_FILTER_OUT += ../../../../src/KTX-Software/lib/dfdutils%

MY_SOURCES_EXTENSION += .cpp .c .cc .S .cxx
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
