/*
// [API version]
@par API Version
v4.0
// [API version]
// [Code version]
v4.0.0-23-g354f640e
// [Code version]
*/
#define LIBKTX_VERSION v4.0.0-23-g354f640e
#define LIBKTX_DEFAULT_VERSION v4.0.__default__