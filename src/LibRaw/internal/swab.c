/*
  Copyright 2008-2020 LibRaw LLC (info@libraw.org)

LibRaw is free software; you can redistribute it and/or modify
it under the terms of the one of two licenses as you choose:

1. GNU LESSER GENERAL PUBLIC LICENSE version 2.1
   (See file LICENSE.LGPL provided in LibRaw distribution archive for details).

2. COMMON DEVELOPMENT AND DISTRIBUTION LICENSE (CDDL) Version 1.0
   (See file LICENSE.CDDL provided in LibRaw distribution archive for details).

   This file is generated from Dave Coffin's dcraw.c
   dcraw.c -- Dave Coffin's raw photo decoder
   Copyright 1997-2010 by Dave Coffin, dcoffin a cybercom o net

   Look into dcraw homepage (probably http://cybercom.net/~dcoffin/dcraw/)
   for more information
*/

#include "swab.h"

#ifdef __ANDROID__

#include <stdint.h>
#include <stddef.h>

#include <asm/byteorder.h>

void swab(const void *from, void*to, ssize_t n)
{
    if (n < 0)
        return;

    for (ssize_t i = 0; i < (n/2)*2; i += 2)
    {
#ifdef __arch__swab16
        *((uint16_t*)to+i) = __arch__swab16(*((uint16_t*)from+i));
#else
        uint16_t val = *((uint16_t*)from+i);
        uint16_t hi = (val & 0xFF) << 8;
        uint16_t lo = (val >> 8) & 0xFF;
        *((uint16_t*)to+i) = (hi | lo);
#endif
    }
}
#endif
