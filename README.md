##compile

1.asm tools
```
At windows need add tools to PATH 

tools/asm/nasm-2.13.02
tools/asm/vsyasm-1.3.0-win64

Visual Studio asm configure nasm vsyasm need .props files.
We need copy config files to BuildCustomizations directory.

cp /tools/asm/asm-visual-studio/* to
windows  7  2012 Program Files (x86)\MSBuild\Microsoft.Cpp\v4.0\V110\BuildCustomizations
windows 10  2017 Microsoft Visual Studio\2017\Community\Common7\IDE\VC\VCTargets\BuildCustomizations
windows 10  2019 Microsoft Visual Studio\2019\Enterprise\MSBuild\Microsoft\VC\v160\BuildCustomizations
```

2.ENV
```
mm-make is script compile tool.
We need add MM_HOME to PATH.

The path like:
------------
F:\mm\mm-make
F:\mm\mm-libx
------------

MM_HOME=<TargetToMasterDirectory>
like   MM_HOME=F:\mm

The new version not use OpenGL, ignore this.
POWERVR_SDK_HOME=target to you last version PowerVR_SDK directory
like E:\tools\Imagination\PowerVR_Graphics\PowerVR_SDK\SDK_2017_R1
like sdk\PowerVR\GraphicsSDK\SDK_3.3
```

3.sdk is platform special lib.
```
vld    is windows use for memory leak detection.
vulkan is minimum size VulkanSDK.

windows is version 1.2.170.0 
xcode   need soft link to your VulkanSDK xcframework.
android is already included in AndroidNDK.
```

4.script
```
script\compile\compile_<Platform>.<Suffix> for compile.
like   script\compile\compile_windows.bat
```

5. Visual Studio
```
WindowsSDKPlatformVersion=<WindowsSDKVersion>
VS141COMNTOOLS=*\2017\Community\VC\Auxiliary\Build
like   WindowsSDKPlatformVersion=10.0.17134.0
like   VS141COMNTOOLS=F:\visual-studio\2017\Community\VC\Auxiliary\Build

VS141COMNTOOLS            is use for script compile.
WindowsSDKPlatformVersion is use for IDE .sln compile.

Visual Studio 2017 WindowsSDKPlatformVersion is required.
Visual Studio 2019 WindowsSDKPlatformVersion is optional.

Note: 
Visual Studio 2017 WindowsSDK default is 8.1
You need manual download WindowsSDK 10.0.17134.0.
```

6. Xcode
```
xcpretty is use for script compile.
xcodebuild command is required.

sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer
xcodebuild -version
sudo gem install xcpretty

MoltenVK.xcframework is a bit large.
So sdk/vulkan/lib/xcode/MoltenVK.xcframework is a soft link.

We need create soft-link.
ln -s <YouAppleVulkanSDK> /Users/Shared/tools/VulkanSDK/1.2.170.0/MoltenVK/MoltenVK.xcframework
```

7. Android Studio
```
We use NDK for script compile, so we need some environment PATH.
Android Studio use build.gradle configure app level ndkVersion "21.4.7075529". 

ANDROID_SDK=<YourAndroidSDKInstallationPath>
ANDROID_NDK=*\ndk\21.4.7075529
like   ANDROID_SDK=F:\android\android-sdk
like   ANDROID_NDK=F:\android\android-sdk\ndk\21.4.7075529
```
