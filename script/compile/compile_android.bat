@echo off

set PLATFORM=android
set ACTION=%*

set index=0

set arr[%index%]=zlib                      & set /a index+=1
set arr[%index%]=zstd                      & set /a index+=1
set arr[%index%]=libpng                    & set /a index+=1
set arr[%index%]=jpeg                      & set /a index+=1
set arr[%index%]=openjpeg                  & set /a index+=1
set arr[%index%]=libwebp                   & set /a index+=1
set arr[%index%]=tiff                      & set /a index+=1
set arr[%index%]=giflib                    & set /a index+=1
set arr[%index%]=jxrlib                    & set /a index+=1

set arr[%index%]=LibRaw                    & set /a index+=1
set arr[%index%]=libsquish                 & set /a index+=1
set arr[%index%]=Imath                     & set /a index+=1
set arr[%index%]=openexr                   & set /a index+=1
set arr[%index%]=draco                     & set /a index+=1
set arr[%index%]=dfdutils                  & set /a index+=1
set arr[%index%]=basis_universal           & set /a index+=1
set arr[%index%]=KTX-Software              & set /a index+=1
set arr[%index%]=FreeImage                 & set /a index+=1
set arr[%index%]=glslang                   & set /a index+=1
set arr[%index%]=openal-soft               & set /a index+=1

set arr[%index%]=openssl                   & set /a index+=1
set arr[%index%]=curl                      & set /a index+=1
set arr[%index%]=hiredis                   & set /a index+=1
set arr[%index%]=zookeeper                 & set /a index+=1
set arr[%index%]=rabbitmq                  & set /a index+=1

set arr[%index%]=lua                       & set /a index+=1
set arr[%index%]=toluapp                   & set /a index+=1
set arr[%index%]=sqlite                    & set /a index+=1
set arr[%index%]=cJSON                     & set /a index+=1
set arr[%index%]=protobuf                  & set /a index+=1
set arr[%index%]=protobuf-c                & set /a index+=1

set arr[%index%]=libogg                    & set /a index+=1
set arr[%index%]=libvorbis                 & set /a index+=1

set arr[%index%]=libiconv                  & set /a index+=1
set arr[%index%]=lz4                       & set /a index+=1

set arr[%index%]=spine-c                   & set /a index+=1

:: echo %index%

set _BuildCMD=%MM_HOME%/mm-make/script/bat/compile-libx.bat
@echo y | call %_BuildCMD% arr %index% %PLATFORM% %ACTION% 

GOTO :EOF
