#!/bin/bash

PLATFORM=linux
ACTION=$@

#################################################
index=0
#################################################
arr[index++]='zlib'
arr[index++]='zstd'
arr[index++]='libpng'
arr[index++]='jpeg'
arr[index++]='openjpeg'
arr[index++]='libwebp'
arr[index++]='tiff'
arr[index++]='giflib'
arr[index++]='jxrlib'

arr[index++]='LibRaw'
arr[index++]='libsquish'
arr[index++]='Imath'
arr[index++]='openexr'
arr[index++]='draco'
arr[index++]='dfdutils'
arr[index++]='basis_universal'
arr[index++]='KTX-Software'
arr[index++]='FreeImage'
arr[index++]='glslang'
arr[index++]='openal-soft'

arr[index++]='openssl'
arr[index++]='curl'
arr[index++]='hiredis'
arr[index++]='zookeeper'
arr[index++]='rabbitmq'

arr[index++]='lua'
arr[index++]='toluapp'
arr[index++]='sqlite'
arr[index++]='cJSON'
arr[index++]='protobuf'
arr[index++]='protobuf-c'

arr[index++]='libogg'
arr[index++]='libvorbis'

arr[index++]='libiconv'
arr[index++]='lz4'

arr[index++]='spine-c'
#################################################
source "${MM_HOME}/mm-make/script/bash/compile-libx.bash"

BuildCompile arr ${index} ${PLATFORM} ${ACTION}
