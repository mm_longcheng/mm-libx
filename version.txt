```
pthread                        2.91

zlib                           1.2.11
zstd                           1.4.5
libpng                         1.6.37
jpeg                           9b
openjpeg                       2.4.0
libwebp                        1.2.0
tiff                           4.3.0
giflib                         5.2.1
jxrlib                         1.1

LibRaw                         0.20.2
libsquish                      1.15
Imath                          3.0.3
openexr                        3.0.3
draco                          1.4.1
dfdutils                       1.3
basis_universal                v1.15
KTX-Software                   4.0.0
FreeImage                      3.18.0
glslang                        11.6.0
openal-soft                    1.22.0

openssl                        1.1.1j
curl                           7.78.0
hiredis                        1.0.0
zookeeper                      3.7.0
rabbitmq                       0.11.0

lua                            5.4.2
tolua++                        1.0.92
sqlite                         3.36.0
cJSON                          1.7.15
protobuf                       3.17.3
protobuf-c                     1.4.0

libiconv                       1.16
lz4                            1.9.3

libogg                         1.3.5
libvorbis                      1.3.7

spine-c                        4.1
```


